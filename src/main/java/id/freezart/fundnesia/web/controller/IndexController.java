package id.freezart.fundnesia.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author isman
 * @since 7/7/20
 */
@Controller
public class IndexController {

    @GetMapping(value = {"/","/index"})
    private ModelAndView index(){
        ModelAndView modelAndView = new ModelAndView("index");
        return modelAndView;
    }

}
