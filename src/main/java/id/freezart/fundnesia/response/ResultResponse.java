package id.freezart.fundnesia.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author isman
 * @since 7/7/20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResultResponse<T> {

    private String code;

    private String message;

    private String description;

    private T data;

}
