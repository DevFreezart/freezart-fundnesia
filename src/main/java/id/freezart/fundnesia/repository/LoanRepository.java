package id.freezart.fundnesia.repository;

import id.freezart.fundnesia.entity.Loan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author isman
 * @since 7/7/20
 */
@Repository
public interface LoanRepository extends CrudRepository<Loan,String> {
}
