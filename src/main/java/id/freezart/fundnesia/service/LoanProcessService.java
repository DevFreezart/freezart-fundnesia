package id.freezart.fundnesia.service;

import id.freezart.fundnesia.messaging.model.LoanModel;

/**
 * @author isman
 * @since 7/7/20
 */
public interface LoanProcessService {

    void process(LoanModel model);

}
