package id.freezart.fundnesia.service.impl;

import id.freezart.fundnesia.constant.LoanConstant;
import id.freezart.fundnesia.entity.Loan;
import id.freezart.fundnesia.enums.Status;
import id.freezart.fundnesia.exceptions.LoanNotFoundException;
import id.freezart.fundnesia.messaging.model.LoanModel;
import id.freezart.fundnesia.repository.LoanRepository;
import id.freezart.fundnesia.service.LoanProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * @author isman
 * @since 7/7/20
 */
@Service
public class LoanProcessServiceImpl implements LoanProcessService {

    @Autowired
    private LoanRepository repository;

    @Override
    public void process(LoanModel model) {

        Optional<Loan> checkLoan = repository.findById(model.getId());

        if (!checkLoan.isPresent()){
            throw new LoanNotFoundException("Loan Not found.!");
        }

        Loan loan = checkLoan.get();

        BigDecimal tenure = BigDecimal.valueOf(loan.getTenure());
        BigDecimal fee = LoanConstant.FEE_PERCENTATION.multiply(tenure).multiply(loan.getTicketSize());
        BigDecimal totalLoan = fee.add(loan.getTicketSize());
        BigDecimal installMentPerMonth = totalLoan.divide(tenure);

        loan.setFee(fee);
        loan.setTotalLoan(totalLoan);
        loan.setInstallmentPerMonth(installMentPerMonth);
        loan.setStatus(Status.SUBMIT);

        repository.save(loan);

    }
}
