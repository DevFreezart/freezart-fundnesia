package id.freezart.fundnesia.service.impl;

import id.freezart.fundnesia.entity.Loan;
import id.freezart.fundnesia.enums.Status;
import id.freezart.fundnesia.messaging.model.LoanModel;
import id.freezart.fundnesia.properties.RabbitMQProperties;
import id.freezart.fundnesia.repository.LoanRepository;
import id.freezart.fundnesia.request.LoanRequest;
import id.freezart.fundnesia.response.ResultResponse;
import id.freezart.fundnesia.service.LoanAddService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @author isman
 * @since 7/7/20
 */
@Service
@Slf4j
public class LoanAddServiceImpl implements LoanAddService {

    @Autowired
    private LoanRepository loanRepository;

    @Autowired
    @Qualifier("amqpTemplate")
    private RabbitTemplate amqpTemplate;

    @Autowired
    private RabbitMQProperties properties;

    @Override
    public ResultResponse loanAdd(LoanRequest request) {

        Loan loan = new Loan();
        loan.setTenure(request.getTenure());
        loan.setTicketSize(request.getTicketSize());
        loan.setFee(BigDecimal.ZERO);
        loan.setTotalLoan(BigDecimal.ZERO);
        loan.setInstallmentPerMonth(BigDecimal.ZERO);
        loan.setStatus(Status.PROCESS);

        loanRepository.save(loan);

        sendToLoanPorcess(LoanModel.builder()
                .id(loan.getId())
                .build());

        ResultResponse<LoanRequest> response = new ResultResponse();
        response.setCode("201");
        response.setMessage("Loan Process");
        response.setDescription("Loan is success created, system will process your loan request");
        response.setData(request);

        return response;
    }

    private void sendToLoanPorcess(LoanModel model){
        log.debug("Before send to Rabbit: {}",model.toString());
        amqpTemplate.setExchange(properties.getExchangeName());
        amqpTemplate.setRoutingKey(properties.getRoutingKey());
        amqpTemplate.convertAndSend(model);
        log.debug("After send to Rabbit: {}",model.toString());
    }
}
