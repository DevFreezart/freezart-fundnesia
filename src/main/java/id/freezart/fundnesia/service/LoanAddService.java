package id.freezart.fundnesia.service;

import id.freezart.fundnesia.request.LoanRequest;
import id.freezart.fundnesia.response.ResultResponse;

/**
 * @author isman
 * @since 7/7/20
 */
public interface LoanAddService {

    ResultResponse loanAdd(LoanRequest request);

}
