package id.freezart.fundnesia.resource;

import id.freezart.fundnesia.request.LoanRequest;
import id.freezart.fundnesia.response.ErrorResponse;
import id.freezart.fundnesia.response.ResultResponse;
import id.freezart.fundnesia.service.LoanAddService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author isman
 * @since 7/7/20
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class LoanResource {

    @Autowired
    private LoanAddService loanService;

    @CrossOrigin
    @PostMapping("/loan/add")
    public ResponseEntity loanAddPost(@RequestBody @Valid LoanRequest request, Errors errors){

        if (errors.hasErrors()) {
            List<ErrorResponse> errorResponseList = errors.getAllErrors().stream().map(err -> ErrorResponse.builder()
                    .title("Bad Request")
                    .detail(err.getDefaultMessage())
                    .build()).collect(Collectors.toList());

            ResultResponse<List<ErrorResponse>> response = new ResultResponse();
            response.setCode("400");
            response.setData(errorResponseList);
            response.setMessage("Failed Add");
            response.setDescription("Failed add loan, please input correct value");
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        try{
            ResultResponse response = loanService.loanAdd(request);

            return new ResponseEntity(response,HttpStatus.CREATED);

        }catch (Exception ex){
            log.error("Error at LoanResource.loanAdd",ex);
            ResultResponse<LoanRequest> response = new ResultResponse();
            response.setCode("500");
            response.setMessage("Internal Server Error");
            response.setDescription("Internal Server Error, please contact developer.!!");
            response.setData(request);
            return new ResponseEntity(response,HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

}
