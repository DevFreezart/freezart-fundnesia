package id.freezart.fundnesia.constant;

import java.math.BigDecimal;

/**
 * @author isman
 * @since 7/7/20
 */
public interface LoanConstant {
    BigDecimal FEE_PERCENTATION = BigDecimal.valueOf(0.02);
}
