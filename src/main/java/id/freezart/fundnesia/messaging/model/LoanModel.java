package id.freezart.fundnesia.messaging.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author isman
 * @since 7/7/20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoanModel {
    private String id;
}
