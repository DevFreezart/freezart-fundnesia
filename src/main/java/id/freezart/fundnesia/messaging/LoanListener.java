package id.freezart.fundnesia.messaging;

import id.freezart.fundnesia.exceptions.LoanNotFoundException;
import id.freezart.fundnesia.messaging.model.LoanModel;
import id.freezart.fundnesia.service.LoanProcessService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author isman
 * @since 7/7/20
 */
@Component
@Slf4j
public class LoanListener {

    @Autowired
    private LoanProcessService loanProcessService;

    @RabbitListener(queues = "${freezart.fundnesia.messaging.queueName}")
    public void listen(LoanModel message) {

        log.debug("FromQueue: " + message.toString());

        try {
            loanProcessService.process(message);
        }catch (LoanNotFoundException lnfe){
            log.error("LoanListener.listen",lnfe.getMessage());
        }catch (Exception e){
            log.error("LoanListener.listen",e);
        }

    }
}
