package id.freezart.fundnesia.request;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author isman
 * @since 7/7/20
 */
@Data
public class LoanRequest {

    @NotNull(message = "Tenure can't null")
    @Min(value = 1L, message = "Tenure min 1")
    private Integer tenure;

    @NotNull(message = "Ticket size can't null")
    @Min(value = 1L, message = "Ticket size min 1")
    private BigDecimal ticketSize;

}
