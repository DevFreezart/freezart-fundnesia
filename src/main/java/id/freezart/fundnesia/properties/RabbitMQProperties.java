package id.freezart.fundnesia.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author isman
 * @since 7/7/20
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "freezart.fundnesia.messaging")
public class RabbitMQProperties {
    private String queueName;
    private String exchangeName;
    private String routingKey;
}