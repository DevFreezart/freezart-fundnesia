package id.freezart.fundnesia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FreezartFundnesiaApplication {

    public static void main(String[] args) {
        SpringApplication.run(FreezartFundnesiaApplication.class, args);
    }

}
