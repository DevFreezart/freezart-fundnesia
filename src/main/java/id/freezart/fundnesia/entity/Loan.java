package id.freezart.fundnesia.entity;

import id.freezart.fundnesia.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author isman
 * @since 7/7/20
 */
@Entity
@Table(name = "loans")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Loan {

    @Id
    @GeneratedValue(
            generator = "uuid2"
    )
    @GenericGenerator(
            name = "uuid2",
            strategy = "uuid2"
    )
    @Column(name = "id")
    private String id;

    @Column(name = "tenure")
    private Integer tenure;

    @Column(name = "ticket_size")
    private BigDecimal ticketSize;

    @Column(name = "fee")
    private BigDecimal fee;

    @Column(name = "total_loan")
    private BigDecimal totalLoan;

    @Column(name = "installment_per_month")
    private BigDecimal installmentPerMonth;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

}
