package id.freezart.fundnesia.enums;

/**
 * @author isman
 * @since 7/7/20
 */
public enum  Status {

    PROCESS,SUBMIT,CANCEL,ERROR;

}
